﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssigmentOOAD
{
    public class ClsCompany
    {
        protected string companyID { get; set; }
        private string companyName { get; set; }
        private string companyAddress { get; set; }
        private string companyPhone { get; set; }
        private string companyEmail { get; set; }
        private string companyWebsite { get; set; }
        public ClsCompany()
        {
        }
        public ClsCompany(string companyID)
        {
            this.companyID = companyID;
        }
        public ClsCompany(string companyID, string companyName, string companyAddress, string companyPhone, string companyEmail, string companyWebsite)
        {
            this.companyID = companyID;
            this.companyName = companyName;
            this.companyAddress = companyAddress;
            this.companyPhone = companyPhone;
            this.companyWebsite = companyWebsite;
        }
        
    }
   

}
