﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssigmentOOAD
{
    public class ClsExport:ClsProduct
    {
        private string exportID { get; set; }
        private string exportDate { get; set; }
        private double exportAmount { get; set; }
        private double price { get; set; }
        private double totalprice { get; set; }
        private string recieverName { get; set; }
        private string recieverPhone { get; set; }
        private string user { get; set; }
        public ClsExport(string ExportID,string user,string productID, string ExportDate, double ExportAmount,double price,double totalprice, string recieverName, string recieverPhone)
            : base(productID)
        {
            this.exportID = ExportID;
            this.exportDate = ExportDate;
            this.exportAmount = ExportAmount;
            this.price = price;
            this.totalprice = totalprice;
            this.recieverName = recieverName;
            this.recieverPhone = recieverPhone;
            this.user = user;
        }
        public string ExportID
        {
            get { return this.exportID; }
        }
        public string User
        {
            get { return this.user; }
        }
        public string ProductID
        {
            get { return this.productID; }
        }
        public string ExportDate
        {
            get { return this.exportDate; }
        }
        public double ExportAmount
        {
            get { return this.exportAmount; }
        }
        public double ExportPrice
        {
            get { return this.price; }
        }
        public double TotalPrice
        {
            get { return this.totalprice; }
        }
        public string ReciverName
        {
            get { return this.recieverName; }
        }
        public string RecieverPhone
        {
            get { return this.recieverPhone; }
        }
    }
}
