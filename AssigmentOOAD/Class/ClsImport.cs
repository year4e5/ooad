﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssigmentOOAD
{
   public class ClsImport:ClsProduct
    {
        private string importID { get; set; }
        private string importDate { get; set; }
        private string userName { get; set; }
        private double importAmount { get; set; }
        private double price { get; set; }
        private double totalPrice { get; set; }
        private string deliveryName { get; set; }
        private string deliveryPhone { get; set; }
        public ClsImport( string importID,string userName,string productID,string companyID, string importDate,double importAmount, double price, double totalPrice 
            , string deliveryName, string deliveryPhone)
            :base(companyID,productID)
        {
            this.importID = importID;
            this.importDate = importDate;
            this.importAmount = importAmount;
            this.price = price;
            this.totalPrice = totalPrice;
            this.deliveryName = deliveryName;
            this.deliveryPhone = deliveryPhone;
            this.userName = userName;
        }
        public string ImportID
        {
            get { return this.importID; }
        }
        public string ImportDate
        {
            get { return this.importDate; }
        }
        public double ImportAmount
        {
            get { return this.importAmount; }
        }
        public double Price
        {
            get { return this.price; }
        }
        public double TotalPrice
        {
            get { return this.totalPrice; }
        }
        public string DeliveryName
        {
            get { return this.deliveryName; }
        }
        public string DeliveryPhone
        {
            get { return this.deliveryPhone; }
        }
        public string UserName
        {
            get { return this.userName; }
        }
        public string ProductID
        {
            get{return base.productID;}
        }
        public string CompanyID
        {
            get { return base.companyID; }
        }
    }
}
