﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssigmentOOAD
{
    class ClsIncome: ClsProduct
    {
        private string day { get; set; }
        private string month { get; set; }
        private string year { get; set; } 
        private double qty { get; set; }
        //public ClsIncome(string day, string month, string year, string productID, double amount)
        //    : base(productID)
        //{
        //    this.day = day;
        //    this.month = month;
        //    this.year = year;
        //    this.amount = amount;
        //}      
        public string DAY { get { return day; } set { day = value; } }
        public string MONTH { get { return month; } set { month = value; } }
        public string YEAR { get { return year; } set { year = value; } }
        public double QTY { get { return qty; } set { qty = value; } }

    }
}
