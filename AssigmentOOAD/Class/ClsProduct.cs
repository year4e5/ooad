﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssigmentOOAD
{
    public class ClsProduct:ClsCompany
    {
        protected string productID { get; set; }
        private string productName { get; set; }
        private string productType { get; set; }
        private string productPrice { get; set; }
        public ClsProduct()
        {
        }
        public ClsProduct(string companyID, string productID)
        {
            this.companyID = companyID;
            this.productID = productID;
        }
        public ClsProduct(string productID)
        {
            this.productID = productID;
        }
        public ClsProduct(string companyID,string productID, string productName, string productType, string productPrice):base(companyID)
        {
            this.productName = productName;
            this.productType = productType;
            this.productPrice = productPrice;
        }




        public string PRODUCTID { get { return productID; } set { productID = value; } }
        public string PRODUCTNAME { get { return productName; } set { productName = value; } }
        public string PRODUCTTYPE { get { return productType; } set { productType = value; } }
        public string PRODUCTPRICE { get { return productPrice; } set { productPrice = value; } }
    }
}
