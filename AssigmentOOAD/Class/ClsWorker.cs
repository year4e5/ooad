﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssigmentOOAD
{
    public class ClsWorker
    {
        private string id { get; set; }
        private string name { get; set; }
        private string gender { get; set; }
        private DateTime dob { get; set; }
        private string pob { get; set; }
        private string currentAdress { get; set; }
        private string phone { get; set; }
        private double salary { get; set; }
        private bool isDelete { get; set; }

        //ClsWorker()
        //{
        //}

        public string ID             { get { return id; }  set { this.id = value; } }
       public string NAMES           { get { return name; } set { this.name = value; } }
       public string GENDER          { get { return gender; } set { this.gender = value; } }
       public DateTime DOB             { get { return dob; } set { this.dob = value; } }
       public string POB             { get { return pob; } set { this.pob = value; } }
       public string CURRENTADDRESS  { get { return currentAdress; } set { this.currentAdress = value; } }
       public string PHONE           { get { return phone; } set { this.phone = value; } }
       public double SALARY          { get { return salary; } set { this.salary = value; } }
       public bool ISDELETE { get { return isDelete; } set { this.isDelete = value; } }
       public ClsWorker(string id, string name, string gender, DateTime dob, string pob, string currentaddress, string phone, double salary, bool isdelete)
       {
           this.id = id;
           this.name = name;
           this.gender = gender;
           this.dob = dob;
           this.pob = pob;
           this.currentAdress = currentaddress;
           this.phone = phone;
           this.salary = salary;
           this.isDelete = isdelete;
       }

        //public Dictionary<string, dynamic> CLASS_MEMBER
        //{
        //    get
        //    {
        //        Dictionary<string, dynamic> value = new Dictionary<string, dynamic>();
        //        value.Add(this.ID.ToString(), this.id);
        //        value.Add(this.NAME.ToString(), this.name);
        //        value.Add(this.GENDER.ToString(), this.gender);
        //        value.Add(this.DOB.ToString(), this.dob);
        //        value.Add(this.POB.ToString(), this.pob);
        //        value.Add(this.CURRENTADDRESS.ToString(), this.currentAdress);
        //        value.Add(this.PHONE.ToString(), this.phone);
        //        value.Add(this.SALARY.ToString(), this.salary);
        //        value.Add(this.ISDELETE.ToString(), this.isDelete);

        //        return value;
        //    }

        //    set
        //    {
        //        this.id = value["id"];
        //        this.name = value["name"];
        //        this.gender = value["gender"];
        //        this.dob = value["dob"];
        //        this.pob = value["pob"];
        //        this.currentAdress = value["currentAdress"];
        //        this.phone = value["phone"];
        //        this.salary = value["salary"];
        //        this.isDelete = value["isDelete"];
        //    } 
        //}
    }
}
