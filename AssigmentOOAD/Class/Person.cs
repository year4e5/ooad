﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssigmentOOAD
{
    public class Person
    {
        protected string ID { get; set; }
        protected string Name { get; set; }
        protected string Gender { get; set; }
        protected string DOB { get; set; }
        protected string POB { get; set; }
        protected string CurrentAddress { get; set; }
        protected string Phone { get; set; }
        protected string Salary { get; set; }
        public Person()
        {
        }
        public Person(string ID, string Name, string Gender, string DOB, string POB, string CurrentAddress, string Phone, string Salary)
        {
            this.ID = ID;
            this.Name = Name;
            this.Gender = Gender;
            this.DOB = DOB;
            this.POB = POB;
            this.CurrentAddress = CurrentAddress;
            this.Phone = Phone;
            this.Salary = Salary;
        }
    }
}
