﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssigmentOOAD
{
    class User:Person
    {
        private string username { get; set; }
        private string password { get; set; }
        public User(string ID, string Name, string Gender, string DOB, string POB,
            string CurrentAddress, string Phone, string Salary, string username, string password)
            : base(ID, Name, Gender, DOB, POB, CurrentAddress, Phone, Salary)
        {
            this.username = username;
            this.password = password;
        }
    }
}
