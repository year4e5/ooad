﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AssigmentOOAD
{
    public partial class Form1 : Form
    {
        
        public Form1(string username)
        {
            InitializeComponent();
            lblUser.Text = username;
            HOME();
            btnHome.Click += btnHome_Click;
            btnImport.Click += btnImport_Click;
            btnExport.Click += btnExport_Click;
            btnIncome.Click += btnIncome_Click;
            btnUserAccount.Click += btnUserAccount_Click;
            btnInformation.Click += btnInformation_Click;
            btnExit.Click += btnExit_Click;
        }
        private void HOME()
        {
            Home h = new Home();
            h.Dock = DockStyle.Fill;
            pnlSlide.Controls.Clear();
            pnlSlide.Controls.Add(h);
        }
        void btnExit_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.Close();
        }

        void btnInformation_Click(object sender, EventArgs e)
        {
            Information information = new Information();
            information.Dock = DockStyle.Fill;
            pnlSlide.Controls.Clear();
            pnlSlide.Controls.Add(information);
        }

        void btnUserAccount_Click(object sender, EventArgs e)
        {
            UserAccount useraccount = new UserAccount();
            useraccount.Dock = DockStyle.Fill;
            pnlSlide.Controls.Clear();
            pnlSlide.Controls.Add(useraccount);
        }

        void btnIncome_Click(object sender, EventArgs e)
        {
            Income income = new Income();
            income.Dock = DockStyle.Fill;
            pnlSlide.Controls.Clear();
            pnlSlide.Controls.Add(income);
        }

        void btnExport_Click(object sender, EventArgs e)
        {
            Export export = new Export(lblUser.Text);
            export.Dock = DockStyle.Fill;
            pnlSlide.Controls.Clear();
            pnlSlide.Controls.Add(export);
        }

        void btnImport_Click(object sender, EventArgs e)
        {
            Import import = new Import(lblUser.Text);
            import.Dock = DockStyle.Fill;
            pnlSlide.Controls.Clear();
            pnlSlide.Controls.Add(import);
        }

        void btnHome_Click(object sender, EventArgs e)
        {
            Home h = new Home();
            h.Dock = DockStyle.Fill;
            pnlSlide.Controls.Clear();
            pnlSlide.Controls.Add(h);
        }
         
    }
}
