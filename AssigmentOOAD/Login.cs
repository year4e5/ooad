﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AssigmentOOAD
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
            btnLogin.Click += btnLogin_Click;
        }

        void btnLogin_Click(object sender, EventArgs e)
        {
            Operation cnn = new Operation();
            Status st=cnn.login(txtusername.Text, txtpassword.Text);
            if (st.retcode.Equals("1"))
            {
                Form1 frm= new Form1(txtusername.Text);
                frm.Show();
                
            }
            else
                MessageBox.Show("Wrong username or password", "Login", MessageBoxButtons.OK, MessageBoxIcon.Error);
            
        }
    }
}
