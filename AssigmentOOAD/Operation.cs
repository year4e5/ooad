﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Reflection;
using AssigmentOOAD;

namespace AssigmentOOAD
{
    public class Operation
    {
        public Dictionary<string, string> companysource = new Dictionary<string, string>();
        public Dictionary<string, string> productsource = new Dictionary<string, string>();
        
        SqlConnection cnn = null;
        SqlCommand cmd;
        SqlDataAdapter rd;
        SqlDataReader dr;
        public Operation()
        {
            string connect = ConfigurationManager.ConnectionStrings["Connect"].ConnectionString;
            cnn = new SqlConnection(connect);
            cnn.Open();
            string initailCompany = "Select * from V_CompanyName";
            string initailProduct = "SELECT  * FROM V_ProductName";
            cmd = new SqlCommand();
            cmd.Connection = cnn;
            cmd.CommandText = initailCompany;
            cmd.ExecuteNonQuery();

            SqlDataReader rd = cmd.ExecuteReader();
            while (rd.Read())
            {

                companysource.Add(rd.GetString(0), rd.GetString(1));
            }
            rd.Close();

            cmd.CommandText = initailProduct;
            cmd.ExecuteNonQuery();
            rd = cmd.ExecuteReader();
            while (rd.Read())
            {
                productsource.Add(rd.GetString(0), rd.GetString(1));
            }
            rd.Close();
        }
        public double getNumberItem(string id)
        {
            double number = 0;
            cmd.CommandText = "SELECT Amount FROM tblDailyProduct WHERE productID='" + id + "'";
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                number = dr.GetDouble(0);
            }
            dr.Close();
            return number;
        }
        //public List<ClsWorker> Get_Worker()
        //{
        //    List<ClsWorker> Values = new List<ClsWorker>();
        //    Dictionary<string, object> V_worker;
        //    string connect = ConfigurationManager.ConnectionStrings["Connect"].ConnectionString;
        //    cnn = new SqlConnection(connect);
        //    cnn.Open();
        //    string v_workercmd = "Select * from V_Worker"; 
        //    cmd = new SqlCommand();
        //    cmd.Connection = cnn;
        //    cmd.CommandText = v_workercmd;
        //    cmd.ExecuteNonQuery();

        //    SqlDataReader rd = cmd.ExecuteReader(); 
        //    while (rd.Read())
        //    {
        //        V_worker = new Dictionary<string, object>();
        //        for (int i = 0; i < rd.FieldCount; i++)
        //        {
        //            try
        //            {
        //                string FIELDS = rd.GetName(i).ToUpper();
        //                object value = rd.GetValue(i);
        //                V_worker.Add(FIELDS,value);
        //            }
        //            catch (Exception)
        //            { 
        //            }
        //        }
        //        Values.Add(Tools.ToObject<ClsWorker>(V_worker));
        //    }
        //    rd.Close();
        //    return Values;
        //}
        public Status login(string userName, string password)
        {
            Status st = new Status();
            cmd = new SqlCommand("sp_Login", cnn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@UserName", SqlDbType.VarChar)).Value = userName;
            cmd.Parameters.Add(new SqlParameter("@Password", SqlDbType.VarChar)).Value = password;
            SqlDataAdapter rd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            rd.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                st.retcode = "1";
                st.message = "Login success";
            }
            else
            {
                st.retcode = "0";
                st.message = "Login unsuccess";
            }
            return st;
        }
        public DataTable viewImport()
        {
            cmd.CommandText = "SELECT * FROM V_ImportView";
            cmd.CommandType = CommandType.Text;
            DataTable dt = new DataTable();
            rd = new SqlDataAdapter(cmd);
            rd.Fill(dt);
            return dt;
        }
        public DataTable ViewUser()
        {
            cmd.CommandText = "SELECT * FROM V_User";
            cmd.CommandType = CommandType.Text;
            DataTable dt = new DataTable();
            rd = new SqlDataAdapter(cmd);
            rd.Fill(dt);
            return dt;
        }
        public DataTable viewCompany()
        {
            cmd.CommandText = "SELECT * FROM V_Company";
            cmd.CommandType = CommandType.Text;
            DataTable dt = new DataTable();
            rd = new SqlDataAdapter(cmd);
            rd.Fill(dt);
            return dt;
        }
        //public DataTable viewProduct()
        //{
        //    cmd.CommandText = "SELECT * FROM V_Product";
        //    cmd.CommandType = CommandType.Text;
        //    DataTable dt = new DataTable();
        //    Dictionary<string, object> a = new Dictionary<string, object>();
        //    rd = new SqlDataAdapter(cmd);
        //    rd.Fill(dt);
        //    a = (Dictionary<string, object>)dt;
        //    return dt;
        //}
        public DataTable ViewWorker()
        {
            cmd.CommandText = "SELECT * FROM V_Worker";
            cmd.CommandType = CommandType.Text;
            DataTable dt = new DataTable();
            rd = new SqlDataAdapter(cmd);
            rd.Fill(dt);
            return dt;
        }
        public Status insertExport(ClsExport e)
        {
            Status st = new Status();
            ClsExport exp = e;
            try
            {
                cmd = new SqlCommand("sp_InsertExport", cnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@exportID", SqlDbType.VarChar)).Value = exp.ExportID;
                cmd.Parameters.Add(new SqlParameter("@user", SqlDbType.VarChar)).Value = exp.User;
                cmd.Parameters.Add(new SqlParameter("@pid", SqlDbType.VarChar)).Value = exp.ProductID;
                cmd.Parameters.Add(new SqlParameter("@d", SqlDbType.Date)).Value = Tool.ConvertToDate(exp.ExportDate);
                cmd.Parameters.Add(new SqlParameter("@a", SqlDbType.Decimal)).Value = exp.ExportAmount;
                cmd.Parameters.Add(new SqlParameter("@p", SqlDbType.Decimal)).Value = exp.ExportPrice;
                cmd.Parameters.Add(new SqlParameter("@tp", SqlDbType.Decimal)).Value = exp.TotalPrice;
                cmd.Parameters.Add(new SqlParameter("@rn", SqlDbType.VarChar)).Value = exp.ReciverName;
                cmd.Parameters.Add(new SqlParameter("@rp", SqlDbType.VarChar)).Value = exp.RecieverPhone;
                DateTime dt = DateTime.Now;
                cmd.Parameters.Add(new SqlParameter("@day", SqlDbType.VarChar)).Value = dt.Day;
                cmd.Parameters.Add(new SqlParameter("@m", SqlDbType.VarChar)).Value = dt.Month;
                cmd.Parameters.Add(new SqlParameter("@y", SqlDbType.VarChar)).Value = dt.Year;
                int status = cmd.ExecuteNonQuery();
                if (status != 0)
                {
                    st.retcode = "1";
                    st.message = "Insert Successfuly";
                }
                else
                {
                    st.retcode = "0";
                    st.message = "Insert Unsuccessfuly";
                }
            }
            catch (Exception ex)
            {
                st.retcode = "0";
                st.message = "Unsuccessful Insert";
            }
            return st;
        }
        public string getImportID()
        {
            string id="error";
            cmd.CommandText = "SELECT * FROM V_ImportID";
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                id = dr.GetString(0);
            }
            dr.Close();
            return id;
        }
        
      

       
        public Status Transaction_Worker(ClsWorker w)
        {
           
            Status st = new Status();
            ClsWorker worker = w;
            try
            {
                cmd = new SqlCommand("sp_InsertWorker", cnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@i", SqlDbType.VarChar)).Value = worker.ID;
                cmd.Parameters.Add(new SqlParameter("@n", SqlDbType.VarChar)).Value = worker.NAMES;
                cmd.Parameters.Add(new SqlParameter("@G", SqlDbType.VarChar)).Value = worker.GENDER;
                cmd.Parameters.Add(new SqlParameter("@d", SqlDbType.Date)).Value = worker.DOB;
                cmd.Parameters.Add(new SqlParameter("@p", SqlDbType.VarChar)).Value = worker.POB;
                cmd.Parameters.Add(new SqlParameter("@c", SqlDbType.VarChar)).Value = worker.CURRENTADDRESS;
                cmd.Parameters.Add(new SqlParameter("@pl", SqlDbType.VarChar)).Value = worker.PHONE;
                cmd.Parameters.Add(new SqlParameter("@s", SqlDbType.Decimal)).Value = worker.SALARY; 
                cmd.Parameters.Add(new SqlParameter("@isD", SqlDbType.Bit)).Value = worker.ISDELETE;
                
                int status = cmd.ExecuteNonQuery();
                if (status != 0)
                {

                    st.retcode = "1";
                    st.message = "Successfuly";
                }
                else
                {
                    st.retcode = "0";
                    st.message = "Unsuccessfuly";
                }
            }
            catch (Exception ex)
            {
                st.retcode = "0";
                st.message = "Unsuccessful";
            }
            return st;
        }
        private void CheckAmount(string ID, double amount)
        {
            cmd = new SqlCommand("sp_CheckDaily", cnn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@id", SqlDbType.VarChar)).Value = ID;
            SqlParameter output = new SqlParameter();
            output.ParameterName = "@Amount";
            output.SqlDbType = SqlDbType.Float;
            output.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(output);
            cmd.ExecuteNonQuery();
            double re = Convert.ToDouble(output.Value.ToString());
            if (re < amount)
                throw new Exception("Amount in stock is not enough");
        }
        public Status insertImport(ClsImport imp)
        {
            Status st = new Status();
            ClsImport im = imp;
            try
            {
                cmd = new SqlCommand("sp_InsertImport", cnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@i", SqlDbType.VarChar)).Value = im.ImportID;
                cmd.Parameters.Add(new SqlParameter("@u", SqlDbType.VarChar)).Value = im.UserName;
                cmd.Parameters.Add(new SqlParameter("@p", SqlDbType.VarChar)).Value = im.ProductID;
                cmd.Parameters.Add(new SqlParameter("@c", SqlDbType.VarChar)).Value = im.CompanyID;
                cmd.Parameters.Add(new SqlParameter("@d", SqlDbType.Date)).Value = Tool.ConvertToDate(im.ImportDate);
                cmd.Parameters.Add(new SqlParameter("@a", SqlDbType.Decimal)).Value = im.ImportAmount;
                cmd.Parameters.Add(new SqlParameter("@pr", SqlDbType.Decimal)).Value = im.Price;
                cmd.Parameters.Add(new SqlParameter("@tp", SqlDbType.Decimal)).Value = im.TotalPrice;
                cmd.Parameters.Add(new SqlParameter("@dn", SqlDbType.VarChar)).Value = im.DeliveryName;
                cmd.Parameters.Add(new SqlParameter("@dp", SqlDbType.VarChar)).Value = im.DeliveryPhone;
                int status = cmd.ExecuteNonQuery();
                if (status != 0)
                {
                    st.retcode = "1";
                    st.message = "Successful Insert";
                }
            }
            catch (Exception ex)
            {
                st.retcode = "0";
                st.message = "Unsuccessful Insert";
            }
            return st;
        }
        public Status deleteImport(string impID)
        {
            Status st = new Status();
            try
            {
                cmd = new SqlCommand("sp_DeleteImport", cnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@impID", SqlDbType.VarChar)).Value = impID;
                int status = cmd.ExecuteNonQuery();
                if (status != 0)
                {
                    st.retcode = "1";
                    st.message = "Delete Successful";
                }
            }
            catch (Exception ex)
            {
                st.retcode = "0";
                st.message = "Delete Unsuccessful";
            }
            return st;
        }
        public Status updateImport(ClsImport imp)
        {
            Status st = new Status();
            //ClsImport im = imp;
            //try
            //{
            //    cmd = new SqlCommand("sp_UpdateImport", cnn);
            //    cmd.CommandType = CommandType.StoredProcedure;
            //    cmd.Parameters.Add(new SqlParameter("@impID", SqlDbType.VarChar)).Value = im.ImportID;
            //    cmd.Parameters.Add(new SqlParameter("@productID", SqlDbType.VarChar)).Value = im.ProductID;
            //    cmd.Parameters.Add(new SqlParameter("@companyID", SqlDbType.VarChar)).Value = im.CompanyID;
            //    cmd.Parameters.Add(new SqlParameter("@impDate", SqlDbType.DateTime)).Value = Tool.ConvertToDate(im.ImportDate);
            //    cmd.Parameters.Add(new SqlParameter("@impAmount", SqlDbType.Decimal)).Value = im.ImportAmount;
            //    cmd.Parameters.Add(new SqlParameter("@price", SqlDbType.Decimal)).Value = im.Price;
            //    cmd.Parameters.Add(new SqlParameter("@totalprice", SqlDbType.Decimal)).Value = im.TotalPrice;
            //    cmd.Parameters.Add(new SqlParameter("@deliveryName", SqlDbType.VarChar)).Value = im.DeliveryName;
            //    cmd.Parameters.Add(new SqlParameter("@DeliveryPhone", SqlDbType.VarChar)).Value = im.DeliveryPhone;
            //    cmd.Parameters.Add(new SqlParameter("@CarID", SqlDbType.VarChar)).Value = im.CarID;
            //    int status = cmd.ExecuteNonQuery();
            //    if (status != 0)
            //    {
            //        st.retcode = "1";
            //        st.message = "updated Successful";
            //    }
            //}
            //catch (Exception ex)
            //{
            //    st.retcode = "0";
            //    st.message = "updated Unsuccessful";
            //}
            return st;
        }

    }
}
