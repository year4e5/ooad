﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AssigmentOOAD
{
    public static class Tools
    {
        public static T ToObject<T>(this IDictionary<string, object> source) 
            where T: class,new ()
        {
            var someObject = new T();
            var someObjectType = someObject.GetType();

            foreach (var item in source)
            {
                someObjectType
                         .GetProperty(item.Key)
                         .SetValue(someObject, item.Value, null);
            }

            return someObject;
        }

        public static IDictionary<string, object> AsDictionary(this object source, BindingFlags bindingAttr = BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance)
        {
            try
            {
                return source.GetType().GetProperties(bindingAttr).ToDictionary
                (
                    propInfo => propInfo.Name,
                    propInfo => propInfo.GetValue(source, null)
                );
            }
            catch (Exception ex) {
                return new Dictionary<string, object>();

            }

        }


        public static List<T> DgvSourceToClass<T>(System.Windows.Forms.DataGridView DataGridView)
        where T : class, new()
        {
            List<T> value = new List<T>();
            Dictionary<string, object> TempDictionary = new Dictionary<string, object>();
            var dgvSource = DataGridView.DataSource;
            value = (List<T>)dgvSource;
            return value;
        }
    }
}
