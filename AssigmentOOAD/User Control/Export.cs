﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AssigmentOOAD
{
    public partial class Export : UserControl
    {
        Operation cnn=null;
        private string user;
        private double instock = 0;
        public Export(string user)
        {
            InitializeComponent();
            this.user = user;
            initail();
            btnAdd.Click += btnAdd_Click;
            btnEdit.Click += btnEdit_Click;
            btnDelete.Click += btnDelete_Click;
            btnSave.Click += btnSave_Click;
            dgv.SelectionChanged += dgv_SelectionChanged;
            cboProductName.SelectedIndexChanged += cboProductName_SelectedIndexChanged;
            txtExportAmount.KeyUp += txtprice_KeyUp;
            txtprice.KeyUp += txtprice_KeyUp;
        }
        void txtprice_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtprice.Text) || string.IsNullOrEmpty(txtExportAmount.Text)) return;
                double total = double.Parse(txtprice.Text) * double.Parse(txtExportAmount.Text);
                txtTotalprice.Text = total.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Input", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        void cboProductName_SelectedIndexChanged(object sender, EventArgs e)
        {
            instock = cnn.getNumberItem(cboProductName.SelectedValue.ToString());
            txtInstock.Text = instock.ToString();
        }

        void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                for (int index = 0; index < dgv.Rows.Count; index++)
                {
                    string expid = dgv.Rows[index].Cells[0].Value.ToString();
                    string productID = dgv.Rows[index].Cells[1].Value.ToString();
                    string expDate = dgv.Rows[index].Cells[2].Value.ToString();
                    string recieverName = dgv.Rows[index].Cells[3].Value.ToString();
                    string recieverPhone = dgv.Rows[index].Cells[4].Value.ToString();
                    double expAmount =double.Parse(dgv.Rows[index].Cells[5].Value.ToString());
                    double expprice = double.Parse(dgv.Rows[index].Cells[6].Value.ToString());
                    double exptotalprice = double.Parse(dgv.Rows[index].Cells[7].Value.ToString());
                    ClsExport exp = new ClsExport(expid, user, productID, expDate, expAmount, expprice, exptotalprice, recieverName, recieverPhone);
                    Status st = cnn.insertExport(exp);
                    MessageBox.Show(st.message, "Save Export", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        void btnDelete_Click(object sender, EventArgs e)
        {
            dgv.Rows.RemoveAt(dgv.CurrentCell.RowIndex);
        }

        void dgv_SelectionChanged(object sender, EventArgs e)
        {
            if (dgv.CurrentRow.Index == 0) return;
            int idx = dgv.CurrentCell.RowIndex;
            string exportID=dgv.Rows[idx].Cells[0].Value.ToString();
            string productID=dgv.Rows[idx].Cells[1].Value.ToString();
            string exportDate=dgv.Rows[idx].Cells[2].Value.ToString();
            string RecieverName=dgv.Rows[idx].Cells[3].Value.ToString();
            string ReciverPhone=dgv.Rows[idx].Cells[4].Value.ToString();
            double Amount=(double)dgv.Rows[idx].Cells[5].Value;
            double Price = (double)dgv.Rows[idx].Cells[6].Value;
            double Total = (double)dgv.Rows[idx].Cells[7].Value;
        }

        void btnEdit_Click(object sender, EventArgs e)
        {
            int idx=dgv.CurrentCell.RowIndex;
            string exportID = txtExportID.Text;
            string productID = cboProductName.SelectedValue.ToString();
            string exportDate = txtExportDate.Value.ToShortDateString();
            string exportReciverName = txtRecieverName.Text;
            string exportReciverPhone = txtRecieverPhone.Text;
            double exportAmount = double.Parse(txtExportAmount.Text);
            double exportPrice = double.Parse(txtprice.Text);
            double exportTotal = double.Parse(txtTotalprice.Text);
            dgv.Rows.RemoveAt(idx);
            dgv.Rows.Add(exportID, productID, exportDate, exportReciverName, exportReciverPhone, exportAmount, exportPrice, exportTotal);
        }
        void btnAdd_Click(object sender, EventArgs e)
        {
            
            try
            {
                string exportID = txtExportID.Text;
                string productID = cboProductName.SelectedValue.ToString();
                string exportDate = txtExportDate.Value.ToShortDateString();
                string exportReciverName = txtRecieverName.Text;
                string exportReciverPhone = txtRecieverPhone.Text;
                double exportAmount = double.Parse(txtExportAmount.Text);
                if (instock <exportAmount) throw new Exception("Amount export more than instock");
                double exportPrice = double.Parse(txtprice.Text);
                double exportTotal = double.Parse(txtTotalprice.Text);
                if (instock<=0) throw new Exception("Amount export more than instock");
                dgv.Rows.Add(exportID, productID, exportDate, exportReciverName, exportReciverPhone, exportAmount, exportPrice, exportTotal);
                instock -= exportAmount;
                txtInstock.Text = instock.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Wrong Input or Amount larger than in stock", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }
        private void initail()
        {
            cnn = new Operation();
            cboProductName.DataSource = new BindingSource(cnn.productsource, null);
            if (cnn.productsource.Count > 0)
            {
                cboProductName.DisplayMember = "Value";
                cboProductName.ValueMember = "Key";
                dgv.Columns.Add("colExportID", "ExportID");
                dgv.Columns.Add("colprouductName", "Product Name");
                dgv.Columns.Add("colExportDate", "Export Date");
                dgv.Columns.Add("colExportRecieverName", "Reviever Name");
                dgv.Columns.Add("colExportRecieverPhone", "Reciever Phone");
                dgv.Columns.Add("colExportAmount", "Amount");
                dgv.Columns.Add("colExportPrice", "Price");
                dgv.Columns.Add("colExportTotal", "Total");
            }
            instock = cnn.getNumberItem(cboProductName.SelectedValue.ToString());
            txtInstock.Text = instock.ToString();
        }
        
    }
}
