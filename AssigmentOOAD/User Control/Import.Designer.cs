﻿namespace AssigmentOOAD
{
    partial class Import
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtImportID = new System.Windows.Forms.TextBox();
            this.cboCompanyName = new System.Windows.Forms.ComboBox();
            this.cboProductID = new System.Windows.Forms.ComboBox();
            this.txtImportDate = new System.Windows.Forms.DateTimePicker();
            this.txtImportAmount = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDeliveryPhone = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDeliveryName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtImportTotalPrice = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtImportPrice = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.label10 = new System.Windows.Forms.Label();
            this.btnDetailCompany = new System.Windows.Forms.Button();
            this.btnDetailProduct = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnADD = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.txtRecord = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(98, 51);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "Import ID";
            // 
            // txtImportID
            // 
            this.txtImportID.Enabled = false;
            this.txtImportID.Location = new System.Drawing.Point(226, 48);
            this.txtImportID.Name = "txtImportID";
            this.txtImportID.Size = new System.Drawing.Size(231, 27);
            this.txtImportID.TabIndex = 1;
            // 
            // cboCompanyName
            // 
            this.cboCompanyName.FormattingEnabled = true;
            this.cboCompanyName.Location = new System.Drawing.Point(699, 48);
            this.cboCompanyName.Name = "cboCompanyName";
            this.cboCompanyName.Size = new System.Drawing.Size(217, 28);
            this.cboCompanyName.TabIndex = 2;
            // 
            // cboProductID
            // 
            this.cboProductID.FormattingEnabled = true;
            this.cboProductID.Location = new System.Drawing.Point(699, 97);
            this.cboProductID.Name = "cboProductID";
            this.cboProductID.Size = new System.Drawing.Size(217, 28);
            this.cboProductID.TabIndex = 3;
            // 
            // txtImportDate
            // 
            this.txtImportDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.txtImportDate.Location = new System.Drawing.Point(226, 98);
            this.txtImportDate.Name = "txtImportDate";
            this.txtImportDate.Size = new System.Drawing.Size(231, 27);
            this.txtImportDate.TabIndex = 4;
            // 
            // txtImportAmount
            // 
            this.txtImportAmount.Location = new System.Drawing.Point(226, 145);
            this.txtImportAmount.Name = "txtImportAmount";
            this.txtImportAmount.Size = new System.Drawing.Size(231, 27);
            this.txtImportAmount.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(548, 51);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 21);
            this.label2.TabIndex = 5;
            this.label2.Text = "Company Name";
            // 
            // txtDeliveryPhone
            // 
            this.txtDeliveryPhone.Location = new System.Drawing.Point(699, 191);
            this.txtDeliveryPhone.Name = "txtDeliveryPhone";
            this.txtDeliveryPhone.Size = new System.Drawing.Size(217, 27);
            this.txtDeliveryPhone.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(94, 244);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 21);
            this.label3.TabIndex = 7;
            this.label3.Text = "Total Price";
            // 
            // txtDeliveryName
            // 
            this.txtDeliveryName.Location = new System.Drawing.Point(699, 142);
            this.txtDeliveryName.Name = "txtDeliveryName";
            this.txtDeliveryName.Size = new System.Drawing.Size(217, 27);
            this.txtDeliveryName.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(98, 194);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 21);
            this.label4.TabIndex = 9;
            this.label4.Text = "Price";
            // 
            // txtImportTotalPrice
            // 
            this.txtImportTotalPrice.Enabled = false;
            this.txtImportTotalPrice.Location = new System.Drawing.Point(226, 241);
            this.txtImportTotalPrice.Name = "txtImportTotalPrice";
            this.txtImportTotalPrice.Size = new System.Drawing.Size(231, 27);
            this.txtImportTotalPrice.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(98, 148);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(117, 21);
            this.label5.TabIndex = 11;
            this.label5.Text = "Import Amount";
            // 
            // txtImportPrice
            // 
            this.txtImportPrice.Location = new System.Drawing.Point(226, 191);
            this.txtImportPrice.Name = "txtImportPrice";
            this.txtImportPrice.Size = new System.Drawing.Size(231, 27);
            this.txtImportPrice.TabIndex = 14;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(548, 100);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 21);
            this.label6.TabIndex = 13;
            this.label6.Text = "Product ID";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(98, 104);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(93, 21);
            this.label7.TabIndex = 15;
            this.label7.Text = "Import Date";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(548, 194);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(115, 21);
            this.label8.TabIndex = 17;
            this.label8.Text = "Delivery Phone";
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Location = new System.Drawing.Point(98, 342);
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.Size = new System.Drawing.Size(913, 171);
            this.dgv.TabIndex = 19;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(548, 145);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(113, 21);
            this.label10.TabIndex = 20;
            this.label10.Text = "Delivery Name";
            // 
            // btnDetailCompany
            // 
            this.btnDetailCompany.Location = new System.Drawing.Point(936, 48);
            this.btnDetailCompany.Name = "btnDetailCompany";
            this.btnDetailCompany.Size = new System.Drawing.Size(75, 28);
            this.btnDetailCompany.TabIndex = 21;
            this.btnDetailCompany.Text = "Detail";
            this.btnDetailCompany.UseVisualStyleBackColor = true;
            // 
            // btnDetailProduct
            // 
            this.btnDetailProduct.Location = new System.Drawing.Point(936, 96);
            this.btnDetailProduct.Name = "btnDetailProduct";
            this.btnDetailProduct.Size = new System.Drawing.Size(75, 28);
            this.btnDetailProduct.TabIndex = 22;
            this.btnDetailProduct.Text = "Detail";
            this.btnDetailProduct.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe UI", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(521, 9);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(111, 35);
            this.label11.TabIndex = 23;
            this.label11.Text = "IMPORT";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panel1.Controls.Add(this.btnSave);
            this.panel1.Controls.Add(this.btnPrint);
            this.panel1.Controls.Add(this.btnDelete);
            this.panel1.Controls.Add(this.btnEdit);
            this.panel1.Controls.Add(this.btnADD);
            this.panel1.Location = new System.Drawing.Point(102, 287);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(909, 37);
            this.panel1.TabIndex = 24;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(176, 0);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(117, 37);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            // 
            // btnPrint
            // 
            this.btnPrint.Location = new System.Drawing.Point(792, 0);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(117, 37);
            this.btnPrint.TabIndex = 4;
            this.btnPrint.Text = "Print";
            this.btnPrint.UseVisualStyleBackColor = true;
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(586, 0);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(117, 37);
            this.btnDelete.TabIndex = 2;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(373, 0);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(117, 37);
            this.btnEdit.TabIndex = 1;
            this.btnEdit.Text = "Edit";
            this.btnEdit.UseVisualStyleBackColor = true;
            // 
            // btnADD
            // 
            this.btnADD.Location = new System.Drawing.Point(0, 0);
            this.btnADD.Name = "btnADD";
            this.btnADD.Size = new System.Drawing.Size(117, 37);
            this.btnADD.TabIndex = 0;
            this.btnADD.Text = "ADD";
            this.btnADD.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(822, 522);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(42, 21);
            this.label12.TabIndex = 26;
            this.label12.Text = "Total";
            // 
            // txtRecord
            // 
            this.txtRecord.Location = new System.Drawing.Point(887, 519);
            this.txtRecord.Name = "txtRecord";
            this.txtRecord.Size = new System.Drawing.Size(124, 27);
            this.txtRecord.TabIndex = 25;
            // 
            // Import
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(228)))), ((int)(((byte)(242)))));
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtRecord);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.btnDetailProduct);
            this.Controls.Add(this.btnDetailCompany);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtImportPrice);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtImportTotalPrice);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtDeliveryName);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtDeliveryPhone);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtImportAmount);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtImportDate);
            this.Controls.Add(this.cboProductID);
            this.Controls.Add(this.cboCompanyName);
            this.Controls.Add(this.txtImportID);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Segoe UI", 9.818182F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Import";
            this.Size = new System.Drawing.Size(1133, 578);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtImportID;
        private System.Windows.Forms.ComboBox cboCompanyName;
        private System.Windows.Forms.ComboBox cboProductID;
        private System.Windows.Forms.DateTimePicker txtImportDate;
        private System.Windows.Forms.TextBox txtImportAmount;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDeliveryPhone;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtDeliveryName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtImportTotalPrice;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtImportPrice;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnDetailCompany;
        private System.Windows.Forms.Button btnDetailProduct;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnADD;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtRecord;
        private System.Windows.Forms.Button btnSave;
    }
}
