﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace AssigmentOOAD
{
    public partial class Import : UserControl
    {
        Operation cnn;
        private string user;
        public Import(string user)
        {
            InitializeComponent();
            this.user = user;
            initail();
            btnADD.Click += btnADD_Click;
            btnEdit.Click += btnEdit_Click;
            btnDelete.Click += btnDelete_Click;
            btnSave.Click += btnSave_Click;
            dgv.SelectionChanged += dgv_SelectionChanged;
            txtImportPrice.KeyUp += txtImportPrice_KeyUp;
            txtImportAmount.KeyUp += txtImportPrice_KeyUp;
            
        }
        void txtImportPrice_KeyUp(object sender, KeyEventArgs e)
        {
            if (string.IsNullOrEmpty(txtImportPrice.Text)) return;
            if (string.IsNullOrEmpty(txtImportAmount.Text)) return;
            txtImportTotalPrice.Text = (double.Parse(txtImportPrice.Text) * double.Parse(txtImportAmount.Text)).ToString();
        }

        void btnSave_Click(object sender, EventArgs e)
        {
            for (int index = 0; index < dgv.Rows.Count; index++)
            {
            string id = dgv.Rows[index].Cells[0].Value.ToString();
            string cname = dgv.Rows[index].Cells[1].Value.ToString();
            string cproduct = dgv.Rows[index].Cells[2].Value.ToString();
            string impDate=dgv.Rows[index].Cells[3].Value.ToString();
            double amount =Convert.ToDouble(dgv.Rows[index].Cells[4].Value.ToString());
            double price = Convert.ToDouble(dgv.Rows[index].Cells[5].Value.ToString());
            double totalprice =Convert.ToDouble(dgv.Rows[index].Cells[6].Value.ToString());
            string dName= dgv.Rows[index].Cells[7].Value.ToString();
            string dPhone = dgv.Rows[index].Cells[8].Value.ToString();
            ClsImport imp = new ClsImport(id, user, cproduct, cname, impDate, amount, price, totalprice, dName, dPhone);
            Status st=cnn.insertImport(imp);
            MessageBox.Show(st.message, "Save Import", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        void btnDelete_Click(object sender, EventArgs e)
        {
            if (dgv.CurrentRow.Index == 0) return;
            dgv.Rows.RemoveAt(dgv.CurrentRow.Index);
        }
        void dgv_SelectionChanged(object sender, EventArgs e)
        {
            if (dgv.CurrentRow.Index == 0) return;
            int index = dgv.CurrentCell.RowIndex;
            //txtImportID.Text = dgv.Rows[index].Cells[0].Value.ToString();
            cboCompanyName.SelectedItem = dgv.Rows[index].Cells[1].Value.ToString();
            cboProductID.SelectedItem = dgv.Rows[index].Cells[2].Value.ToString();
            txtImportDate.Value =Convert.ToDateTime(dgv.Rows[index].Cells[3].Value.ToString());
            txtImportAmount.Text = dgv.Rows[index].Cells[4].Value.ToString();
            txtImportPrice.Text = dgv.Rows[index].Cells[5].Value.ToString();
            txtImportTotalPrice.Text = dgv.Rows[index].Cells[6].Value.ToString();
            txtDeliveryName.Text = dgv.Rows[index].Cells[7].Value.ToString();
            txtDeliveryPhone.Text = dgv.Rows[index].Cells[8].Value.ToString();
        }

        void btnEdit_Click(object sender, EventArgs e)
        {
            int index = dgv.CurrentCell.RowIndex;
            if (string.IsNullOrEmpty(txtImportID.Text)) return;
            string impID = txtImportID.Text;
            string companyName = cboCompanyName.SelectedValue.ToString();
            string productName = cboCompanyName.SelectedValue.ToString();
            string impDate = txtImportDate.Value.ToShortDateString();
            string impAmount = txtImportAmount.Text;
            string impprice = txtImportPrice.Text;
            string imptotalprice = txtImportTotalPrice.Text;
            string deliveryName = txtDeliveryName.Text;
            string deliveryPhone = txtDeliveryPhone.Text;
            dgv.Rows.RemoveAt(index);
            dgv.Rows.Add(impID, companyName, productName, impDate, impAmount, impprice, deliveryName, deliveryPhone);
        }
        void btnADD_Click(object sender, EventArgs e)
        {
            string impID = txtImportID.Text;
            string companyName = cboCompanyName.SelectedValue.ToString();
            string productName = cboProductID.SelectedValue.ToString();
            string impDate = txtImportDate.Value.ToShortDateString();
            string impAmount = txtImportAmount.Text;
            string impprice = txtImportPrice.Text;
            string imptotalprice = txtImportTotalPrice.Text;
            string deliveryName = txtDeliveryName.Text;
            string deliveryPhone = txtDeliveryPhone.Text;
            dgv.Rows.Add(impID, companyName, productName, impDate, impAmount, impprice,imptotalprice, deliveryName, deliveryPhone);
        }
        private void initail()
        {
            try
            {
                cnn = new Operation();
                if (cnn.companysource.Count > 0)
                {
                    cboCompanyName.DataSource = new BindingSource(cnn.companysource, null);
                    cboCompanyName.DisplayMember = "value";
                    cboCompanyName.ValueMember = "Key";
                    cboProductID.DataSource = new BindingSource(cnn.productsource, null);
                    cboProductID.DisplayMember = "Value";
                    cboProductID.ValueMember = "Key";
                    if (cnn.getImportID() == "error")
                        txtImportID.Text = "i1";
                    else
                    {
                        txtImportID.Text ="i"+(double.Parse(cnn.getImportID().Substring(1, 1))+1).ToString();
                    }
                    dgv.Columns.Add("ImpID", "ImpID");
                    dgv.Columns.Add("CompanyName", "Company Name");
                    dgv.Columns.Add("productName", "product Name");
                    dgv.Columns.Add("impDate", "ImpDate");
                    dgv.Columns.Add("ImportAmount", "Import Amount");
                    dgv.Columns.Add("Price", "Price");
                    dgv.Columns.Add("Total", "Total Price");
                    dgv.Columns.Add("Delivery Name", "Delivery Name");
                    dgv.Columns.Add("Delivery Phone", "Delivery Phone");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error load ID or company", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
