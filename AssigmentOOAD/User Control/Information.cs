﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace AssigmentOOAD
{
    public partial class Information : UserControl
    {
        Operation cnn = null;
        public Information()
        {
            InitializeComponent();
            initail();
            rdoCompany.CheckedChanged += rdoCompany_CheckedChanged;
            //rdoProduct.CheckedChanged += rdoProduct_CheckedChanged;
            rdoWorker.CheckedChanged += rdoWorker_CheckedChanged;
            rdoUser.CheckedChanged += rdoUser_CheckedChanged;
        }

        void rdoUser_CheckedChanged(object sender, EventArgs e)
        {
            dgv.DataSource = new BindingSource(cnn.ViewUser(), null);
        }

        void rdoWorker_CheckedChanged(object sender, EventArgs e)
        {
            dgv.DataSource = new BindingSource(cnn.ViewWorker(),null);
        }

        //void rdoProduct_CheckedChanged(object sender, EventArgs e)
        //{
        //    dgv.DataSource = new BindingSource(cnn.viewProduct(), null);
        //}
        private void initail()
        {
            cnn = new Operation();
        }
        void rdoCompany_CheckedChanged(object sender, EventArgs e)
        {
            dgv.DataSource = new BindingSource(cnn.viewCompany(), null);
        }
         
    }
}
