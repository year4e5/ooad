USE [Stock]
GO

/****** Object:  StoredProcedure [dbo].[sp_InsertWorker]    Script Date: 9/24/2019 9:31:43 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- Batch submitted through debugger: sp_InsertWorker.sql|9|0|C:\Users\User\Documents\SQL Server Management Studio\sp_InsertWorker.sql


ALTER PROC [dbo].[sp_InsertWorker] (@i varchar(max),@n varchar(max),@G varchar(max),@d date,@p varchar(max),@c varchar(max),@pl varchar(max),@s float,@isD bit)

as
Begin 
 if((select COUNT(*) from V_Worker where ID=@i)>0)
	begin 
		Update tblWorker  set Names=@n, Gender=@g, DOB= @d ,POB= @p,CurrentAddress= @c,Phone= @pl,SALARY= @s, isDelete=@isD where ID =@i
	end

else
	begin 
	 INSERT INTO tblWorker VALUES(@i,@n,@G,@d,@p,@c,@pl,@s,0)
	end



End




GO


