USE [Stock]
GO

/****** Object:  StoredProcedure [dbo].[sp_InsertWorker]    Script Date: 9/24/2019 12:17:31 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[sp_InsertWorker] (@i varchar(max),@n varchar(max),@G varchar(max),@d date,@p varchar(max),@c varchar(max),@pl varchar(max),@s float)

as

 INSERT INTO tblWorker VALUES(@i,@n,@G,@d,@p,@c,@pl,@s)

GO


